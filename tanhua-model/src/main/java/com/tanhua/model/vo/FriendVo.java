package com.tanhua.model.vo;

import com.tanhua.model.domain.UserInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FriendVo {
     private Integer id; //用户id
     private String  avatar;  //用户头像
     private String  nickname; //昵称
     private String  gender;  //性别
     private Integer age ;  //年龄
     private String city; //城市
     private String education;  //学历
     private Integer marriage; //婚姻状态
     private Integer matchRate; //匹配度
     private boolean alreadyLove; //是否喜欢ta

     public static FriendVo init(UserInfo userInfo, Integer matchRate , Boolean alreadyLove){
          FriendVo friendVo = new FriendVo();
          BeanUtils.copyProperties(userInfo,friendVo);
          friendVo.setMatchRate(matchRate);
          friendVo.setAlreadyLove(alreadyLove);
          return friendVo;
     }
}