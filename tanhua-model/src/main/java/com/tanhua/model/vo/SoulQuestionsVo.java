package com.tanhua.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SoulQuestionsVo {
    private String id;   //  试题编号
    private String question;   //题目
    private List<SoulOptionsVo> options; //  选项
}