package com.tanhua.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author : RYR
 * @description :  与你相似
 * @createTime : 2022/1/4 12:03
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SimilarYouVo implements Serializable {
    private Integer id;    //  用户编号
    private String avatar;      //  头像
}
