package com.tanhua.model.vo;

import com.tanhua.model.domain.Admin;
import com.tanhua.model.domain.UserInfo;
import com.tanhua.model.mongo.Peachblossom;
import com.tanhua.model.mongo.Visitors;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

/**
 * @author : RYR
 * @description :
 * @createTime : 2022/1/2 15:08
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PeachblossomVo {

    private Integer id;   //  用户id
    private String avatar;   //  头像
    private String nickname;    //  昵称
    private String gender;      //  性别
    private Integer age;         //  年龄
    private String soundUrl;    //  语音地址
    private Integer remainingTimes ;         //  剩余次数

    public  static  PeachblossomVo init(UserInfo userInfo, Peachblossom peachblossom){
        PeachblossomVo peachblossomVo = new PeachblossomVo();
        peachblossomVo.setId(Integer.valueOf(userInfo.getId().toString()));
        peachblossomVo.setAvatar(userInfo.getAvatar());
        peachblossomVo.setNickname(userInfo.getNickname());
        peachblossomVo.setGender(userInfo.getGender());
        peachblossomVo.setAge(userInfo.getAge());
        peachblossomVo.setSoundUrl(peachblossom.getSoundUrl());
        peachblossomVo.setRemainingTimes(peachblossom.getRemainingTimes());


        return peachblossomVo;

    }


}
