package com.tanhua.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
//统计互相喜欢,粉丝,粉丝数量
public class CountVo implements Serializable {

    private Integer eachLoveCount;  //互相喜欢
    private Integer loveCount;       //喜欢
    private Integer fanCount;      //粉丝


}