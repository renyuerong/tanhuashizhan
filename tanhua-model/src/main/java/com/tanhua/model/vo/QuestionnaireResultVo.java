package com.tanhua.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author : RYR
 * @description : 查看结果对象返回的vo对象
 * @createTime : 2022/1/4 11:58
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuestionnaireResultVo implements Serializable {
    private String conclusion;   //  鉴定结果
    private String cover;       //  鉴定图片
    private List<DimensionsVo> dimensions;
    private List<SimilarYouVo> similarYou;
}
