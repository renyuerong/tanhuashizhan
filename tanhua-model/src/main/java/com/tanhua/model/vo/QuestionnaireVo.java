package com.tanhua.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuestionnaireVo {
    private String id;
    private String name;
    private String cover;
    private String level;
    private int star;
    private List<SoulQuestionsVo> questions;
    private int isLock;
    private String reportId;
}