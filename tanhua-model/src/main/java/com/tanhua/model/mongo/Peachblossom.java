package com.tanhua.model.mongo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * @author : RYR
 * @description :  桃花传音实体类
 * @createTime : 2022/1/2 14:38
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "peachblossom")
public class Peachblossom implements Serializable {

    private ObjectId id;
    private Long userId;   //  发语音的用户id，即当前登录用户的id
    private String gender; //  性别
    private String soundUrl;    //  语音地址
    private Long created; 		  //发表时间
    private Integer remainingTimes = 10 ;
}
