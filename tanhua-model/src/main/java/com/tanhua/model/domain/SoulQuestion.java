package com.tanhua.model.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author : RYR
 * @description :  灵魂测试，问题表
 * @createTime : 2022/1/3 14:33
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SoulQuestion implements Serializable {
    private Long id;
    private String stem;            //题目内容
    private Long questionnaireId;   //问卷id
    private Date created;           //创建时间
    private Date updated;              //修改时间
}
