package com.tanhua.model.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author : RYR
 * @description : 灵魂测试，问卷列表
 * @createTime : 2022/1/3 14:30
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Questionnaire implements Serializable {
    private Long id;
    private String level;  //问卷等级 1初级；2中级；3高级
    private String name;    //问卷名称
    private String cover;   //问卷封面名称
    private Integer star;   //题目星级 2,3,5星级
    private Date created;   //创建时间
    private Date updated;   //修改时间
}
