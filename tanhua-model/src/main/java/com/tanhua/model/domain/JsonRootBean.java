/**
  * Copyright 2022 bejson.com 
  */
package com.tanhua.model.domain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * Auto-generated: 2022-01-02 16:36:28
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JsonRootBean {

    private ArrayList<ThisYear> thisYear;
    private ArrayList<LastYear> lastYear;


}