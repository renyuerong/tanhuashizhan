package com.tanhua.model.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: tanhua
 * @description:
 * @author: LWB
 * @create: 2022-01-02 16:28
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Shuju {
    private String title;
    private int amount;

}
