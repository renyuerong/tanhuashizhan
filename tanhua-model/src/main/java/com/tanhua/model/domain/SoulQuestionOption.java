package com.tanhua.model.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author : RYR
 * @description :  灵魂测试，问题选项表
 * @createTime : 2022/1/3 14:34
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SoulQuestionOption implements Serializable {
    private String id;        //选项id
    private String content;   //问题选项
    private String medias;
    private Long questionId; //题目id
    private Integer score;   //得分
    private Date created;    //创建时间
    private Date updated;    //修改时间
}