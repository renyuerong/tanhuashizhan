package com.tanhua.model.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: tanhua
 * @description:
 * @author: LWB
 * @create: 2022-01-02 18:07
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Xixixi {
    private String created;
    private Integer id;
}
