package com.tanhua.model.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author : RYR
 * @description : 封装的结果集对象
 * @createTime : 2022/1/4 10:49
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuestionnaireResult implements Serializable {
    private Long id ;
    private Long questionnaireId ;
    private Long scope ;
    private String cover ;
    private String content;
    private Date created;
    private Date updated;
    private String dimensions;

}
