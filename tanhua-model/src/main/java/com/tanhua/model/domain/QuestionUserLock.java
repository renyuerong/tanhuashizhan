package com.tanhua.model.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author : RYR
 * @description : 灵魂测试，用对对应问卷锁表
 * @createTime : 2022/1/3 14:31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuestionUserLock implements Serializable {
    private Long id;
    private Long userId;          //用户id
    private Long questionnaireId; //问卷id
    private Integer isLock;       //对应锁状态  默认是锁住的0  解锁1
    private Date created;         //创建时间
    private Date updated;         //修改时间

}
