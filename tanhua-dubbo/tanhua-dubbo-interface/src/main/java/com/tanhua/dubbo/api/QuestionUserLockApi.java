package com.tanhua.dubbo.api;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.model.domain.QuestionUserLock;

import java.util.List;

/**
 * @author : RYR
 * @description : 问卷锁表
 * @createTime : 2022/1/3 15:03
 */
public interface QuestionUserLockApi {

    Integer count(Long userId);

    List<QuestionUserLock> findById(Long userId);
    //  找到对应的锁内容
    QuestionUserLock findStatus(Long userId, Long questionID);

    void update(QuestionUserLock questionUserLock);
}
