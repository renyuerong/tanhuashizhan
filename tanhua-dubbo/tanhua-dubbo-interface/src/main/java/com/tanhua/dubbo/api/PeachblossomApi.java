package com.tanhua.dubbo.api;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.model.mongo.Peachblossom;

public interface PeachblossomApi  {
    //  保存语音数据
    Boolean saveUrl(Peachblossom peachblossom);
    //  随机接收语音数据
    Peachblossom findRandomUrl(Long userId,String gender);
    //  删除语音
    void delete(Peachblossom peachblossom);
}
