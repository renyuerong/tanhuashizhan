package com.tanhua.dubbo.api;

import com.tanhua.model.domain.Questionnaire;

import java.util.List;

public interface QuestionnaireApi {

    //  查询每一套试卷
    Questionnaire selectById(Long questionnaireId);

}
