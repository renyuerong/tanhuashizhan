package com.tanhua.dubbo.api;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tanhua.model.domain.UserInfo;
import com.tanhua.model.mongo.UserLike;
import com.tanhua.model.vo.CountVo;

import java.util.List;
import java.util.Map;

public interface UserInfoApi {

    public void save(UserInfo userInfo);

    public void update(UserInfo userInfo);

    //根据id查询
    UserInfo findById(Long id);

    /**
     * 批量查询用户详情
     *    返回值：Map<id,UserInfo>
     */
    Map<Long,UserInfo> findByIds(List<Long> userIds,UserInfo info);

    //分页查询
    IPage findAll(Integer page,Integer pagesize);


    List<UserInfo> findByNickname(List<Long> list, String nickname, Integer page, Integer pagesize);

    Map<Long, UserInfo> findByIds(List<Long> userIds, UserInfo info,Integer page,Integer pagesize);

    Map<Long, UserInfo> findbyIds(List<Long> userIds, UserInfo info);
    //  随机查询推荐的用户
    List<UserInfo> findRandom(String gender);
}
