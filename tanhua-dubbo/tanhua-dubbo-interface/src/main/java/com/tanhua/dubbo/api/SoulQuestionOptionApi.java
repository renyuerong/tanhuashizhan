package com.tanhua.dubbo.api;

import com.tanhua.model.domain.SoulQuestionOption;

import java.util.List;

public interface SoulQuestionOptionApi {
    //  通过问题查询答案，一个问题有三条答案
    List<SoulQuestionOption> findByQuestionnaireOption(Long soulQuestionId);

    Integer findScore(String questionId, String optionId);
}
