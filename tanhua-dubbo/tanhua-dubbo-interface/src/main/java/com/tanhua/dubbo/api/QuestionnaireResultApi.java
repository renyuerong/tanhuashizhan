package com.tanhua.dubbo.api;

import com.tanhua.model.domain.QuestionnaireResult;

/**
 * @author : RYR
 * @description :
 * @createTime : 2022/1/4 10:52
 */
public interface QuestionnaireResultApi {
    //  查询结果类型
    QuestionnaireResult findType(Integer total, Long questionID);

    QuestionnaireResult findById(String id);
}
