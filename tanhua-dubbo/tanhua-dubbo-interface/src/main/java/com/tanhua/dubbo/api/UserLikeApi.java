package com.tanhua.dubbo.api;

import com.tanhua.model.mongo.UserLike;
import com.tanhua.model.vo.CountVo;

import java.util.List;
import java.util.Map;

public interface UserLikeApi {

    //保存或者更新
    Boolean saveOrUpdate(Long userId, Long likeUserId, boolean isLike);

    //客户端喜欢数据统计
    CountVo counts(Long userId);

    //粉丝列表
    List<Long> fans(Long userId, Integer page, Integer pagesize);


    Map<Long, UserLike> userLike(Long userId);
}
