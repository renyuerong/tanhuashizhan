package com.tanhua.dubbo.api;

import com.tanhua.model.domain.Questionnaire;
import com.tanhua.model.domain.SoulQuestion;

import java.util.List;

public interface SoulQuestionApi {
    //  通过问卷的id，查询题目
    List<SoulQuestion> findByQuestionnaire(Long id);
    //  根据问题id得到问卷id
    Long findQuestionnaire(String questionId);
}
