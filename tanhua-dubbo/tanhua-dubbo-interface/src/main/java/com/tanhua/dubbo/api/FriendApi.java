package com.tanhua.dubbo.api;

import com.tanhua.model.mongo.Friend;
import com.tanhua.model.mongo.UserLike;

import java.util.List;

public interface FriendApi {

    //添加好友
    void save(Long userId, Long friendId);

    //查询好友列表
    List<Friend> findByUserId(Long userId, Integer page, Integer pagesize);

    void delete(Long userId, Long likeUserId);

    List<Friend> findUserId(Long userId, Integer page, Integer pagesize);

    //喜欢列表

}
