package com.tanhua.dubbo.api;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.tanhua.dubbo.mappers.QuestionUserLockMapper;
import com.tanhua.model.domain.QuestionUserLock;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author : RYR
 * @description : 问卷锁表
 * @createTime : 2022/1/3 15:04
 */
@DubboService
public class QuestionUserLockImpl implements QuestionUserLockApi{

    @Autowired
    private QuestionUserLockMapper questionUserLockMapper;

    @Override
    public Integer count(Long userId) {
        LambdaQueryWrapper<QuestionUserLock> lqw = new LambdaQueryWrapper<>();
        lqw.eq(QuestionUserLock::getUserId,userId);
        return questionUserLockMapper.selectCount(lqw);
    }

    /**
     * 根据用户id查询对应的锁
     * @param userId
     * @return
     */
    @Override
    public List<QuestionUserLock> findById(Long userId) {
        LambdaQueryWrapper<QuestionUserLock> lqw = new LambdaQueryWrapper<>();
        lqw.eq(QuestionUserLock::getUserId,userId);
        return questionUserLockMapper.selectList(lqw);
    }

    @Override
    public QuestionUserLock findStatus(Long userId, Long questionID) {
        LambdaQueryWrapper<QuestionUserLock> lqw = new LambdaQueryWrapper<>();
        lqw.eq(QuestionUserLock::getUserId,userId).eq(QuestionUserLock::getQuestionnaireId,questionID);
        return questionUserLockMapper.selectOne(lqw);
    }

    /**
     * 修改锁的状态
     * @param questionUserLock
     */
    @Override
    public void update(QuestionUserLock questionUserLock) {
        questionUserLockMapper.updateById(questionUserLock);
    }
}
