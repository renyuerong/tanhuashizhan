package com.tanhua.dubbo.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.model.domain.Questionnaire;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface QuestionnaireMapper extends BaseMapper<Questionnaire> {

}
