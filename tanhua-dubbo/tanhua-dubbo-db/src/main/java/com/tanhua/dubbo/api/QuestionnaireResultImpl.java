package com.tanhua.dubbo.api;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.tanhua.dubbo.mappers.QuestionnaireResultMapper;
import com.tanhua.model.domain.QuestionnaireResult;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author : RYR
 * @description :
 * @createTime : 2022/1/4 10:52
 */
@DubboService
public class QuestionnaireResultImpl implements QuestionnaireResultApi{

    @Autowired
    private QuestionnaireResultMapper questionnaireResultMapper;
    //  查询结果类型
    @Override
    public QuestionnaireResult findType(Integer total, Long questionID) {
//        return questionnaireResultMapper.find(total,questionID);

        LambdaQueryWrapper<QuestionnaireResult> lqw = new LambdaQueryWrapper<>();
        lqw.eq(QuestionnaireResult::getQuestionnaireId,questionID);
        lqw.between(0< total && total <20,QuestionnaireResult::getScope,0,20);
        lqw.between(21< total && total <40,QuestionnaireResult::getScope,21,40);
        lqw.between(41< total && total <55,QuestionnaireResult::getScope,41,55);
        lqw.between(56< total && total <100,QuestionnaireResult::getScope,56,100);
        return questionnaireResultMapper.selectOne(lqw);

    }
    //  根据id查对象
    @Override
    public QuestionnaireResult findById(String id) {
        LambdaQueryWrapper<QuestionnaireResult> lqw = new LambdaQueryWrapper<>();
        lqw.eq(QuestionnaireResult::getId,id);
        return questionnaireResultMapper.selectOne(lqw);
    }

}
