package com.tanhua.dubbo.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.model.domain.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;


import java.util.List;

public interface UserMapper extends BaseMapper<User> {
    @Select(" select count(id) from tb_user ")
    Integer count();

    @Select("select count(id) from tb_user where created>=#{format2} and  created<#{format3};")
    Integer zongshu(@Param("format2") String format2,@Param("format3") String format3);


    @Select("select  id,created from tb_user where  created>=#{ssdd} and  created<=#{eedd} ")
    List<User> xixixi(@Param("ssdd") String ssdd, @Param("eedd") String eedd);
}
