package com.tanhua.dubbo.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.model.domain.SoulQuestion;

/**
 * @author : RYR
 * @description :
 * @createTime : 2022/1/3 15:27
 */
public interface SoulQuestionMapper extends BaseMapper<SoulQuestion> {
}
