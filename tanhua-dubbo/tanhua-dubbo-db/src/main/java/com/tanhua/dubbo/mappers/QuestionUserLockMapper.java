package com.tanhua.dubbo.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.model.domain.QuestionUserLock;

/**
 * @author : RYR
 * @description :
 * @createTime : 2022/1/3 15:05
 */
public interface QuestionUserLockMapper extends BaseMapper<QuestionUserLock> {
}
