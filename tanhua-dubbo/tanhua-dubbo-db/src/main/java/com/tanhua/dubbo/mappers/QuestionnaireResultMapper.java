package com.tanhua.dubbo.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.model.domain.QuestionnaireResult;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface QuestionnaireResultMapper extends BaseMapper<QuestionnaireResult> {
    @Select("select * from tb_questionnaire_result where questionnaire_id = #{questionID} and scope between 0 and 20 ")
    QuestionnaireResult find(Integer total, Long questionID);
}
