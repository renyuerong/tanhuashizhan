package com.tanhua.dubbo.api;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.tanhua.dubbo.mappers.SoulQuestionMapper;
import com.tanhua.model.domain.SoulQuestion;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author : RYR
 * @description :
 * @createTime : 2022/1/3 15:27
 */
@DubboService
public class SoulQuestionImpl implements SoulQuestionApi{

    @Autowired
    private SoulQuestionMapper soulQuestionMapper;
    @Override
    public List<SoulQuestion> findByQuestionnaire(Long id) {
        LambdaQueryWrapper<SoulQuestion> lqw =  new LambdaQueryWrapper<>();
        lqw.eq(SoulQuestion::getQuestionnaireId,id);
        return soulQuestionMapper.selectList(lqw);
    }

    @Override
    public Long findQuestionnaire(String questionId) {
        LambdaQueryWrapper<SoulQuestion> lqw =  new LambdaQueryWrapper<>();
        lqw.eq(SoulQuestion::getId,questionId);
        SoulQuestion soulQuestion = soulQuestionMapper.selectOne(lqw);
        return soulQuestion.getQuestionnaireId();
    }
}
