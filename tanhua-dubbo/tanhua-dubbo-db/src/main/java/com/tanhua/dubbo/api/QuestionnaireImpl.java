package com.tanhua.dubbo.api;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tanhua.dubbo.mappers.QuestionnaireMapper;
import com.tanhua.model.domain.Questionnaire;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author : RYR
 * @description :
 * @createTime : 2022/1/3 15:20
 */
@DubboService
public class QuestionnaireImpl implements QuestionnaireApi{

    @Autowired
    private QuestionnaireMapper questionnaireMapper;
    /**
     * 查询问卷展示图片，名称
     * @return
     */
    @Override
    public Questionnaire selectById(Long questionnaireId) {
        LambdaQueryWrapper<Questionnaire> qw = new LambdaQueryWrapper<>();
        qw.eq(Questionnaire::getId,questionnaireId);
        return questionnaireMapper.selectOne(qw);
    }

}
