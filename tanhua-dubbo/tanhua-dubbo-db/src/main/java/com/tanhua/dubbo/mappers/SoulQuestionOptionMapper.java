package com.tanhua.dubbo.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.model.domain.SoulQuestionOption;

public interface SoulQuestionOptionMapper extends BaseMapper<SoulQuestionOption> {
}
