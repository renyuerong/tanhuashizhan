package com.tanhua.dubbo.api;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.tanhua.dubbo.mappers.SoulQuestionMapper;
import com.tanhua.dubbo.mappers.SoulQuestionOptionMapper;
import com.tanhua.model.domain.SoulQuestionOption;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author : RYR
 * @description :  答案表
 * @createTime : 2022/1/3 15:29
 */
@DubboService
public class SoulQuestionOptionImpl implements SoulQuestionOptionApi{

    @Autowired
    private SoulQuestionOptionMapper soulQuestionOptionMapper;
    @Override
    public List<SoulQuestionOption> findByQuestionnaireOption(Long soulQuestionId) {
        LambdaQueryWrapper<SoulQuestionOption> lqw = new LambdaQueryWrapper<>();
        lqw.eq(SoulQuestionOption::getQuestionId,soulQuestionId);
        return soulQuestionOptionMapper.selectList(lqw);
    }

    /**
     * 根绝题目和答案查询分数
     * @param questionId
     * @param optionId
     * @return
     */
    @Override
    public Integer findScore(String questionId, String optionId) {
        LambdaQueryWrapper<SoulQuestionOption> lqw = new LambdaQueryWrapper<>();
        lqw.eq(SoulQuestionOption::getQuestionId,questionId).eq(SoulQuestionOption::getId,optionId);
        SoulQuestionOption soulQuestionOption = soulQuestionOptionMapper.selectOne(lqw);
        return soulQuestionOption.getScore();
    }
}
