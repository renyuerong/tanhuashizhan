package com.tanhua.dubbo.api;

import cn.hutool.core.collection.CollUtil;
import com.tanhua.model.mongo.Friend;
import com.tanhua.model.mongo.UserLike;
import com.tanhua.model.vo.CountVo;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@DubboService
public class UserLikeApiImpl implements UserLikeApi{

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public Boolean saveOrUpdate(Long userId, Long likeUserId, boolean isLike) {
        try {
            //1、查询数据
            Query query = Query.query(Criteria.where("userId").is(userId).and("likeUserId").is(likeUserId));
            UserLike userLike = mongoTemplate.findOne(query, UserLike.class);
            //2、如果不存在，保存
            if(userLike == null) {
                userLike = new UserLike();
                userLike.setUserId(userId);
                userLike.setLikeUserId(likeUserId);
                userLike.setCreated(System.currentTimeMillis());
                userLike.setUpdated(System.currentTimeMillis());
                userLike.setIsLike(isLike);
                mongoTemplate.save(userLike);
            }else {
                //3、更新
                Update update = Update.update("isLike", isLike)
                        .set("updated",System.currentTimeMillis());
                mongoTemplate.updateFirst(query,update,UserLike.class);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    //客户端喜欢数据统计
    @Override
    public CountVo counts(Long userId) {
        CountVo countVo=new CountVo();
        //1.查询关注的数量
        long lovecount = mongoTemplate.count(Query.query(Criteria.where("userId").is(userId)
                .and("isLike").is(true)), UserLike.class);
        countVo.setLoveCount((int) lovecount);
        //2.查询粉丝的数量
        long fancount = mongoTemplate.count(Query.query(Criteria.where("likeUserId").is(userId)
                .and("isLike").is(true)), UserLike.class);
        countVo.setFanCount((int) fancount);
        //3.统计互相关注的数量
        long count  = mongoTemplate.count(Query.query(Criteria.where("userId").is(userId)), Friend.class);
        countVo.setEachLoveCount((int) count);
        return countVo;
    }

    @Override
    public List<Long> fans(Long userId, Integer page, Integer pagesize) {
        Criteria criteria = Criteria.where("likeUserId").is(userId).and("isLike").is(true);
        Query query = Query.query(criteria).with(Sort.by(Sort.Order.desc("updated")))
                .skip((page-1)*pagesize).limit(pagesize);
        //查询数据
        List<UserLike> likeList = mongoTemplate.find(query, UserLike.class);

        return CollUtil.getFieldValues(likeList,"userId",Long.class);

    }

    @Override
    public Map<Long, UserLike> userLike(Long userId) {
        Criteria criteria = Criteria.where("userId").is(userId);
        List<UserLike> list = mongoTemplate.find(Query.query(criteria), UserLike.class);
        Map<Long, UserLike> map = CollUtil.fieldValueMap(list, "likeUserId");
        return map;
    }


}
