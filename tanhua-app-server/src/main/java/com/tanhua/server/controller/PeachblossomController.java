package com.tanhua.server.controller;

import com.tanhua.model.mongo.Peachblossom;
import com.tanhua.model.vo.PeachblossomVo;
import com.tanhua.server.service.PeachblossomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @author : RYR
 * @description : 桃花传音功能实现
 * @createTime : 2022/1/2 13:49
 */
@RestController
@RequestMapping("/peachblossom")
public class PeachblossomController {

    @Autowired
    private PeachblossomService peachblossomService;

    /**
     * 发送语音
     * @return
     */
    @PostMapping
    public ResponseEntity send(MultipartFile soundFile) throws IOException {
        peachblossomService.send(soundFile);
        return ResponseEntity.ok(null);
    }

    /**
     * 接收语音
     * @return
     */
    @GetMapping
    public PeachblossomVo receive(){
        PeachblossomVo vo = peachblossomService.receive();
        return vo;
    }
}
