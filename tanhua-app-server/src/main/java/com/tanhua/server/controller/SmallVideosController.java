package com.tanhua.server.controller;

import com.tanhua.model.vo.PageResult;
import com.tanhua.server.service.SmallVideosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/smallVideos")
public class SmallVideosController {

    @Autowired
    private SmallVideosService videosService;

    /**
     * 发布视频
     *  接口路径：POST
     *  请求参数：
     *      videoThumbnail：封面图
     *      videoFile：视频文件
     */
    @PostMapping
    public ResponseEntity saveVideos(MultipartFile videoThumbnail, MultipartFile videoFile) throws IOException {
        videosService.saveVideos(videoThumbnail,videoFile);
        return ResponseEntity.ok(null);
    }

    /**
     * 视频列表
     */
    @GetMapping
    public ResponseEntity queryVideoList(@RequestParam(defaultValue = "1")  Integer page,
                                         @RequestParam(defaultValue = "10") Integer pagesize) {
        PageResult result = videosService.queryVideoList(page, pagesize);
        return ResponseEntity.ok(result);
    }

    /**
     * 视频点赞
     * @param id
     * @return
     */
    @PostMapping("/{id}/like")
    public ResponseEntity like(@PathVariable("id") String id){
        videosService.commentLike(id);
        return ResponseEntity.ok(null);
    }

    /***
     * 取消点赞
     * @param id
     * @return
     */
    @PostMapping("/{id}/dislike")
    public ResponseEntity dislike(@PathVariable("id") String id){
        videosService.commentdisLike(id);
        return ResponseEntity.ok(null);
    }

    /**
     * 视频用户关注
     * /smallVideos/:uid/userFocus
     * 提交方式:POST
     */
    @PostMapping("/{id}/userFocus")
    public ResponseEntity followUser(@PathVariable("id") long userId){
        videosService.followUser(userId);
        return ResponseEntity.ok(null);
    }

    /**
     * 取消关注
     * @param userId
     * @return
     */
    /**
     * 取消关注视频作者
     */
    @PostMapping("/{id}/userUnFocus")
    public ResponseEntity userUnFocus(@PathVariable("id") Long followUserId) {
        videosService.unfollowUser(followUserId);
        return ResponseEntity.ok(null);
    }

}