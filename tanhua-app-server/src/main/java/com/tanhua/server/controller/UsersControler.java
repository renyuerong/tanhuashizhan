package com.tanhua.server.controller;

import com.tanhua.model.domain.UserInfo;
import com.tanhua.model.vo.CountVo;
import com.tanhua.model.vo.PageResult;
import com.tanhua.model.vo.UserInfoVo;
import com.tanhua.server.interceptor.UserHolder;
import com.tanhua.server.service.TanhuaService;
import com.tanhua.server.service.UserInfoService;
import com.tanhua.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/users")
public class UsersControler {

    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private UserService userService;
    @Autowired
    private TanhuaService  tanhuaService;

    /**
     * 查询用户资料
     */
    @GetMapping
    public ResponseEntity users(Long userID) {
        if(userID == null) {
            userID = UserHolder.getUserId();
        }
        UserInfoVo userInfo = userInfoService.findById(userID);
        return ResponseEntity.ok(userInfo);
    }

    /**
     * 更新用户资料
     */
    @PutMapping
    public ResponseEntity updateUserInfo(@RequestBody UserInfo userInfo) {
        userInfo.setId(UserHolder.getUserId());
        userInfoService.update(userInfo);
        return ResponseEntity.ok(null);
    }


    /**
     * 修改手机号，发送验证码
     * @return
     */
    @PostMapping("/phone/sendVerificationCode")
    public ResponseEntity sendVerificationCode(){
        userService.sendVerificationCode();
        return ResponseEntity.ok(null);
    }

    /**
     * 修改手机号，校验验证码
     * @return
     */
    @PostMapping("/phone/checkVerificationCode")
    public ResponseEntity checkVerificationCode(@RequestBody Map map){
        String verificationCode = (String) map.get("verificationCode");

        Map checkVerificationCode  = userService.checkVerificationCode(verificationCode);
        System.out.println(map.get("verification"));
        return ResponseEntity.ok(checkVerificationCode);
    }

    /**
     * 修改手机号，保存
     * @return
     */
    @PostMapping("/phone")
    public ResponseEntity updatePhone(@RequestBody Map map){
        String phone = (String) map.get("phone");
        userService.updatePhone(phone);
        return ResponseEntity.ok(null);
    }

    /**
     * 客户端喜欢数据统计
     */
    @GetMapping("/counts")
    public ResponseEntity counts() {
        CountVo countVo = userInfoService.counts();
        return ResponseEntity.ok(countVo);
    }

    /**
     *我的喜欢列表
     *
     */

   @GetMapping("/friends/{type}")
    private ResponseEntity friends(@RequestParam(defaultValue = "1") Integer page,
                                   @RequestParam(defaultValue = "10") Integer pagesize,
                                   String nickname, @PathVariable("type") Integer type){
       PageResult pr = userInfoService.friendsType(page,pagesize,nickname,type);
       return ResponseEntity.ok(pr);
   }

    /**
     * 粉丝喜欢
     */
    @PostMapping("/fans/{uid}")
    public ResponseEntity fans(@PathVariable("uid") String uid){
        tanhuaService.fansLike(Long.valueOf(uid));
        return ResponseEntity.ok(null);
    }

    /**
     * 喜欢取消
     */
    @DeleteMapping("like/{uid}")
    public ResponseEntity deleteLike(@PathVariable("uid") String uid){
        tanhuaService.deleteLike(Long.valueOf(uid));
        return ResponseEntity.ok(null);
    }
}
