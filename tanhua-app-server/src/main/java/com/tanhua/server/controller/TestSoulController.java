package com.tanhua.server.controller;

import com.google.gson.JsonArray;
import com.tanhua.model.vo.QuestionnaireResultVo;
import com.tanhua.model.vo.QuestionnaireVo;
import com.tanhua.server.service.TestSoulService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author : RYR
 * @description : 测试灵魂
 * @createTime : 2022/1/3 14:37
 */
@RestController
@RequestMapping("/testSoul")
public class TestSoulController {

    @Autowired
    private TestSoulService testSoulService;

    /**
     * 问卷展示
     * @return
     */
    @GetMapping
    public ResponseEntity getTestSoul(){
        List<QuestionnaireVo> list = testSoulService.testSoul();
        return ResponseEntity.ok(list);
    }

    /**
     * 提交问卷
     * @param answers
     * @return  返回结果类型的id
     */
    @PostMapping
    public ResponseEntity postTestSoul(@RequestBody Map<String,List<Map>> answers){
        String id = testSoulService.postTestSoul(answers);
        return ResponseEntity.ok(id);
    }

    /**
     * 查看报告
     * @param id  报告的id
     * @return
     */
    @GetMapping("/report/{id}")
    public ResponseEntity lookTestSoul(@PathVariable String id){
        QuestionnaireResultVo vo = testSoulService.lookTestSoul(id);
        return ResponseEntity.ok(vo);
    }


}
