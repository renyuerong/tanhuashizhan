package com.tanhua.server.service;

import cn.hutool.core.collection.CollUtil;
import com.tanhua.dubbo.api.*;
import com.tanhua.model.domain.*;
import com.tanhua.model.vo.*;
import com.tanhua.server.interceptor.UserHolder;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author : RYR
 * @description : 测试灵魂
 * @createTime : 2022/1/3 14:39
 */
@Service
public class TestSoulService {
    @DubboReference
    private QuestionnaireApi questionnaireApi;  //  问卷
    @DubboReference
    private SoulQuestionApi soulQuestionApi;   //      问题题目
    @DubboReference
    private SoulQuestionOptionApi soulQuestionOptionApi;    //  答案
    @DubboReference
    private QuestionUserLockApi questionUserLockApi;    //  试卷锁
    @DubboReference
    private QuestionnaireResultApi questionnaireResultApi;    //   结果对象
    @DubboReference
    private UserInfoApi userInfoApi;

    public List<QuestionnaireVo> testSoul() {
        List<QuestionnaireVo> QuestionnaireVos = new ArrayList();
        //  创建返回对象VO对象

        //  获取当前用户
        Long userId = UserHolder.getUserId();
        //  得到锁对象
        List<QuestionUserLock> lockList = questionUserLockApi.findById(userId);
        //  遍历每个锁对象，通过锁对象
        for (QuestionUserLock questionUserLock : lockList) {
            QuestionnaireVo vo = new QuestionnaireVo();     //  问卷
            //  通过当前用户对应的锁的对象得到对应的试题
            Long questionnaireId = questionUserLock.getQuestionnaireId();
            //  通过问卷id，得到对应的问卷
            Questionnaire questionnaire = questionnaireApi.selectById(questionnaireId);
            // 设置参数，锁对象
            vo.setIsLock(questionUserLock.getIsLock());
            vo.setId(questionnaire.getId().toString());
            vo.setName(questionnaire.getName());
            vo.setCover(questionnaire.getCover());
            vo.setLevel(questionnaire.getLevel());
            vo.setStar(questionnaire.getStar());
            //  通过问卷中的id，查询问题id
            Long questionId = questionnaire.getId();
            //  通过问题id，查询问卷中对应的10个问题
            List<SoulQuestion> soulQuestions = soulQuestionApi.findByQuestionnaire(questionId);
            List<SoulQuestionsVo> questionsList = new ArrayList();
            // 遍历每个问题
            for (SoulQuestion soulQuestion : soulQuestions) {
                SoulQuestionsVo questionsVo = new SoulQuestionsVo();        //  试题vo
                //  设置属性
                questionsVo.setId(soulQuestion.getId().toString());
                questionsVo.setQuestion(soulQuestion.getStem());
                //  通过问题中对应的答案的id，查询每个问题对应的答案
                Long soulQuestionId = soulQuestion.getId();   //  得到这一个问题的id
                //  查询这一个问题对应的3个答案
                List<SoulQuestionOption> byQuestionnaireOption =
                        soulQuestionOptionApi.findByQuestionnaireOption(soulQuestionId);
                //  遍历每个答案，添加到对应的题目中
                List<SoulOptionsVo> optionsList = new ArrayList();
                for (SoulQuestionOption soulQuestionOption : byQuestionnaireOption) {
                    SoulOptionsVo optionsVo = new SoulOptionsVo();              //  答案vo
                    //  设置属性
                    optionsVo.setId(soulQuestionOption.getId());
                    optionsVo.setOption(soulQuestionOption.getContent());
                    //  添加到集合中
                    optionsList.add(optionsVo);
                }
                questionsVo.setOptions(optionsList);
                questionsList.add(questionsVo);
                vo.setQuestions(questionsList);
            }
            QuestionnaireVos.add(vo);
        }
        return QuestionnaireVos;



       //  调用api，查询问卷的展示内容    通过查询可以得到3分问卷的展示面对象

        //  遍历集合，通过集合查询数据

        /*for (Questionnaire questionnaire1 : questionnaire) {
            Long id = questionnaire1.getId();
            //  通过id查询当前水平试卷的题目  得到题目对象
            List<SoulQuestion> soulQuestions = soulQuestionApi.findByQuestionnaire(id);
            List list = new ArrayList();
            //  通过工具类，得到所有的问题的id
            List<Long> soulQuestionIds = CollUtil.getFieldValues(soulQuestions, "id", Long.class);
            for (Long soulQuestionId : soulQuestionIds) {
                //  遍历题目对象得到每个选项的对象
                List<SoulQuestionOption> soulQuestionOptions = soulQuestionOptionApi.findByQuestionnaireOption(soulQuestionId);
                //  问题对应的三个答案

                for (SoulQuestionOption soulQuestionOption : soulQuestionOptions) {
                    //  遍历每个答案，将答案存到vo对象
                    optionsVo.setId(soulQuestionOption.getId());  // 设置选项id
                    optionsVo.setOption(soulQuestionOption.getContent());  //   问题选项
                    list.add(optionsVo);
                }

            }
            //  遍历集合，设置属性
            for (SoulQuestion soulQuestion : soulQuestions) {
                questionsVo.setId(soulQuestion.getId().toString());
                questionsVo.setQuestion(soulQuestion.getStem());
                questionsVo.setOptions(list);
                list2.add(questionsVo);
            }

            //判断问卷访问权限
//获取当前登录用户id


        }*/
       /* List<Object> collect = questionnaire.stream().map(item -> {
            Long id = item.getId();
            //  通过id查询当前水平试卷的题目  得到题目对象
            List<SoulQuestion> soulQuestions = soulQuestionApi.findByQuestionnaire(id);
            List list = new ArrayList();
            //  通过工具类，得到所有的问题的id
            List<Long> soulQuestionIds = CollUtil.getFieldValues(soulQuestions, "id", Long.class);
            for (Long soulQuestionId : soulQuestionIds) {
                //  遍历题目对象得到每个选项的对象
                List<SoulQuestionOption> soulQuestionOptions = soulQuestionOptionApi.findByQuestionnaireOption(soulQuestionId);
                //  问题对应的三个答案

                for (SoulQuestionOption soulQuestionOption : soulQuestionOptions) {
                    //  遍历每个答案，将答案存到vo对象
                    optionsVo.setId(soulQuestionOption.getId());  // 设置选项id
                    optionsVo.setOption(soulQuestionOption.getContent());  //   问题选项
                    list.add(optionsVo);
                }

            }
            //  遍历集合，设置属性
            for (SoulQuestion soulQuestion : soulQuestions) {
                questionsVo.setId(soulQuestion.getId().toString());
                questionsVo.setQuestion(soulQuestion.getStem());
                questionsVo.setOptions(list);
                list2.add(questionsVo);
            }
            return list2;

        }).collect(Collectors.toList());*/
        //判断问卷访问权限
        //获取当前登录用户id
    }

    /**
     * 提交问卷功能
     * @param answers
     */
    public String postTestSoul(Map<String,List<Map>>  answers) {
        List<Map> maps = answers.get("answers");
        Integer total = 0;   //  总分
        Long questionID = 0L;  //  试卷id
        //  遍历作答题目
        for (Map answer : maps) {
            String questionId = (String) answer.get("questionId");     //  试题编号
            String optionId = (String) answer.get("optionId");          //   选项编号
            //  通过试题编号查询试题表，得到问卷的id
            questionID = soulQuestionApi.findQuestionnaire(questionId);
            //  通过试题编号和选项编号进行查询
            total = total + soulQuestionOptionApi.findScore(questionId,optionId);
        }

        //  通过书卷的id和分数来查询对应的类型结果
        QuestionnaireResult questionnaireResult = questionnaireResultApi.findType(total,questionID);

        //  修改锁的状态
        if(questionID+1<=3){
             QuestionUserLock questionUserLock = questionUserLockApi.findStatus(UserHolder.getUserId(),questionID+1);
             //  修改锁的状态
             questionUserLock.setIsLock(0);    //  解锁
             //  修改存入数据库
            questionUserLockApi.update(questionUserLock);
        }
        return questionnaireResult.getId().toString();
    }

    /**
     * 查看报告
     * @param id
     * @return
     */
    public QuestionnaireResultVo lookTestSoul(String id) {
        //  构造返回对象
        QuestionnaireResultVo vo = new QuestionnaireResultVo();

        //  通过报告id查询报告对象
        QuestionnaireResult questionnaireResult = questionnaireResultApi.findById(id);
        vo.setConclusion(questionnaireResult.getContent());
        vo.setCover(questionnaireResult.getCover());
        String dimensions = questionnaireResult.getDimensions();
        String[] split = dimensions.split(",");
        List<DimensionsVo> dlist = new ArrayList<>();
        for (String s : split) {
            String[] split1 = s.split("-");
            DimensionsVo dvo = new DimensionsVo();
            dvo.setKey(split1[0]);
            dvo.setValue(split1[1]);
            dlist.add(dvo);
        }
        vo.setDimensions(dlist);
        //  得到当前用户的id
        Long userId = UserHolder.getUserId();
        //  根据当前用户id查询对象
        UserInfo userInfo = userInfoApi.findById(userId);

        List<UserInfo> infoList = userInfoApi.findRandom(userInfo.getGender());
        List<SimilarYouVo> slist = new ArrayList<>();
        for (UserInfo info : infoList) {
            SimilarYouVo svo = new SimilarYouVo();
            svo.setId(Integer.valueOf(info.getId().toString()));
            svo.setAvatar(info.getAvatar());
            slist.add(svo);
        }
        vo.setSimilarYou(slist);

        return vo;
    }
}
