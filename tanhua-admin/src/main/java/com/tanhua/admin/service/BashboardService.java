package com.tanhua.admin.service;

import com.tanhua.admin.mapper.AnalysisMapper;
import com.tanhua.admin.mapper.LogMapper;
import com.tanhua.dubbo.api.UserApi;

import com.tanhua.model.domain.*;
import com.tanhua.model.vo.AnalysisVo;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @program: tanhua
 * @description:
 * @author: LWB
 * @create: 2022-01-02 10:48
 **/
@Service
public class BashboardService {
    @Autowired
    private AnalysisMapper analysisMapper;
    @DubboReference
    private UserApi userApi;
    @Autowired
    private LogMapper logMapper;

    public AnalysisVo summary() {
        //1累计用户
        Integer cumulativeUsers = userApi.zongshu();
        //2过去30天活跃用户
        long ll = System.currentTimeMillis();
        long l = 86400000L * 30;
        System.out.println(l + "hhhhhh");
        ll = ll - l;
        String format = new SimpleDateFormat("yyyy-MM-dd").format(new Date(ll));
        String type = "0101";
        Integer activePassMonth = logMapper.count(type, format);
        //3过去七天活跃用户
        long l1 = System.currentTimeMillis();
        l1 = l1 - 86400000 * 7;
        String format1 = new SimpleDateFormat("yyyy-MM-dd").format(new Date(l1));

        Integer activePassWeek = logMapper.count(type, format1);
        //4今天新增用户
        long l2 = System.currentTimeMillis();
        String format2 = new SimpleDateFormat("yyyy-MM-dd").format(new Date(l2));
        long l3 = l2 + 86400000;
        String format3 = new SimpleDateFormat("yyyy-MM-dd").format(new Date(l3));

        Integer newUsersToday = userApi.xinzeng(format2, format3);

        //5今日新增涨跌
        long dangqian = System.currentTimeMillis();
        long zuotian = l2 - 86400000;
        String zuotiantime = new SimpleDateFormat("yyyy-MM-dd").format(new Date(zuotian));

        String jintiantime = new SimpleDateFormat("yyyy-MM-dd").format(new Date(dangqian));

        Integer yesToday = userApi.xinzeng(zuotiantime, jintiantime);
        int newUsersTodayRate = newUsersToday - yesToday;
        //6今日登录次数
        long hhh = System.currentTimeMillis();
        String hhhh = new SimpleDateFormat("yyyy-MM-dd").format(new Date(hhh));
        Integer loginTimesToday = logMapper.count(type, hhhh);

        //7今日登录涨跌率

        long zuotiandenglu = hhh - 86400000;
        String zuo = new SimpleDateFormat("yyyy-MM-dd").format(new Date(zuotiandenglu));
        Integer loginTimesyes = logMapper.count(type, hhhh);
        int loginTimesTodayRate = loginTimesToday - loginTimesyes;

        //8今日活跃用户

        int activeUsersToday = logMapper.count(type, hhhh);

        //9今天活跃涨跌率
        Integer a = logMapper.count(type, zuo);
        int activeUsersTodayRate = activeUsersToday - a;

        AnalysisVo analysisVo = new AnalysisVo();
        analysisVo.setCumulativeUsers(cumulativeUsers);
        analysisVo.setActivePassMonth(activePassMonth);
        analysisVo.setActivePassWeek(activePassWeek);
        analysisVo.setNewUsersToday(newUsersToday);
        analysisVo.setNewUsersTodayRate(newUsersTodayRate);
        analysisVo.setLoginTimesToday(loginTimesToday);
        analysisVo.setLoginTimesTodayRate(loginTimesTodayRate);
        analysisVo.setActiveUsersToday(activeUsersToday);
        analysisVo.setActiveUsersTodayRate(activeUsersTodayRate);

        return analysisVo;
    }
    public JsonRootBean increased(String ssdd, String eedd, String sssddd, String eeeddd) {
        ArrayList<ThisYear> thisYears = new ArrayList<>();
        ArrayList<LastYear> lastYears = new ArrayList<>();
        List<Analysis_by_day> active1 = analysisMapper.active(ssdd, eedd);
        List<Analysis_by_day> active2 = analysisMapper.active(sssddd, eeeddd);
        if (active1!=null){
            for (Analysis_by_day analysis_by_day : active1) {
                Integer numActive = analysis_by_day.getNumRegistered();
                Date recordDate = analysis_by_day.getRecordDate();
                String parse = new SimpleDateFormat("yyyy-MM-dd").format(recordDate);
                String substring = parse.substring(5);
                ThisYear thisYear = new ThisYear(substring, numActive);
                thisYears.add(thisYear);


            }
        }
        if (active2!=null){
            for (Analysis_by_day analysis_by_day : active2) {
                Integer numActive = analysis_by_day.getNumRegistered();
                Date recordDate = analysis_by_day.getRecordDate();
                String parse = new SimpleDateFormat("yyyy-MM-dd").format(recordDate);
                String substring = parse.substring(5);
                LastYear lastYear = new LastYear(substring, numActive);
                lastYears.add(lastYear);


            }
        }
        JsonRootBean jsonRootBean = new JsonRootBean();
        jsonRootBean.setThisYear(thisYears);
        jsonRootBean.setLastYear(lastYears);

        return jsonRootBean;


    }

    public JsonRootBean retentionld(String ssdd, String eedd, String sssddd, String eeeddd) {
        ArrayList<ThisYear> thisYears = new ArrayList<>();
        ArrayList<LastYear> lastYears = new ArrayList<>();

        List<Analysis_by_day> retentionld = analysisMapper.retentionld(ssdd, eedd);
        List<Analysis_by_day> retentionld2 = analysisMapper.retentionld(sssddd, eeeddd);
        if (retentionld != null) {
            for (Analysis_by_day analysis_by_day : retentionld) {
                if (analysis_by_day != null) {
                    Integer numRetention1d = analysis_by_day.getNumRetention1d();
                    Date recordDate = analysis_by_day.getRecordDate();
                    String parse = new SimpleDateFormat("yyyy-MM-dd").format(recordDate);
                    String substring = parse.substring(5);
                    ThisYear thisYear = new ThisYear(substring, numRetention1d);
                    thisYears.add(thisYear);

                }
            }

        }
        if (retentionld2 != null) {
            for (Analysis_by_day analysis_by_day : retentionld2) {
                if (analysis_by_day != null) {
                    Integer numRetention1d = analysis_by_day.getNumRetention1d();
                    Date recordDate = analysis_by_day.getRecordDate();
                    String parse = new SimpleDateFormat("yyyy-MM-dd").format(recordDate);
                    String substring = parse.substring(5);
                    LastYear lastYear = new LastYear(substring, numRetention1d);
                    lastYears.add(lastYear);
                }
            }

        }

        JsonRootBean jsonRootBean = new JsonRootBean();
        jsonRootBean.setThisYear(thisYears);
        jsonRootBean.setLastYear(lastYears);
        return jsonRootBean;
    }

    public JsonRootBean active(String ssdd, String eedd, String sssddd, String eeeddd) {
        ArrayList<ThisYear> thisYears = new ArrayList<>();
        ArrayList<LastYear> lastYears = new ArrayList<>();
        List<Analysis_by_day> active1 = analysisMapper.active(ssdd, eedd);
        List<Analysis_by_day> active2 = analysisMapper.active(sssddd, eeeddd);
        if (active1!=null){
            for (Analysis_by_day analysis_by_day : active1) {
                Integer numActive = analysis_by_day.getNumActive();
                Date recordDate = analysis_by_day.getRecordDate();
                String parse = new SimpleDateFormat("yyyy-MM-dd").format(recordDate);
                String substring = parse.substring(5);
                ThisYear thisYear = new ThisYear(substring, numActive);
                thisYears.add(thisYear);


            }
        }
        if (active2!=null){
            for (Analysis_by_day analysis_by_day : active2) {
                Integer numActive = analysis_by_day.getNumActive();
                Date recordDate = analysis_by_day.getRecordDate();
                String parse = new SimpleDateFormat("yyyy-MM-dd").format(recordDate);
                String substring = parse.substring(5);
                LastYear lastYear = new LastYear(substring, numActive);
                lastYears.add(lastYear);


            }
        }
        JsonRootBean jsonRootBean = new JsonRootBean();
        jsonRootBean.setThisYear(thisYears);
        jsonRootBean.setLastYear(lastYears);

        return jsonRootBean;


    }
}
