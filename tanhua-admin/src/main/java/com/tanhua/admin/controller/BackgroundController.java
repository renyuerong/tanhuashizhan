package com.tanhua.admin.controller;

import com.tanhua.admin.service.BashboardService;
import com.tanhua.model.domain.JsonRootBean;
import com.tanhua.model.domain.Shuju;
import com.tanhua.model.domain.ThisYear;
import com.tanhua.model.vo.AnalysisVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * @program: tanhua
 * @description:
 * @author: LWB
 * @create: 2022-01-02 10:16
 **/

@RestController
@RequestMapping("/dashboard")
public class BackgroundController {
    @Autowired
    private BashboardService bashboardService;

    @GetMapping("/summary")
    public ResponseEntity summary(){
        AnalysisVo analysisVo=bashboardService.summary();


        return ResponseEntity.ok(analysisVo);

    }
    @GetMapping("/users")
    public ResponseEntity users(Long sd,Long ed,String type){

        //开始时间
        String ssdd = new SimpleDateFormat("yyyy-MM-dd").format(new Date(sd));
        String[] split = ssdd.split("-");

        //结束时间
        String eedd = new SimpleDateFormat("yyyy-MM-dd").format(new Date(ed));

       String sssddd=new SimpleDateFormat("yyyy-MM-dd").format(new Date(sd-86400000L*365L));
        //去年结束时间
       String eeeddd = new SimpleDateFormat("yyyy-MM-dd").format(new Date(ed-86400000L*365));



        JsonRootBean jsonRootBean = new JsonRootBean();


        if ("101".equals(type)){
            //新增用户
            jsonRootBean = bashboardService.increased(ssdd,eedd,sssddd,eeeddd);
        }
        if ("102".equals(type)){
            //活跃用户
            jsonRootBean=bashboardService.active(ssdd,eedd,sssddd,eeeddd);
        }
        if ("103".equals(type)){
            //次日留存率
          jsonRootBean=bashboardService.retentionld(ssdd,eedd,sssddd,eeeddd);

        }


        return ResponseEntity.ok(jsonRootBean);
    }


}
