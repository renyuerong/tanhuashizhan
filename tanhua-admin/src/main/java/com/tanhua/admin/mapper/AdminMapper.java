package com.tanhua.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.model.domain.Admin;
import org.apache.ibatis.annotations.Mapper;

public interface AdminMapper extends BaseMapper<Admin> {
}
