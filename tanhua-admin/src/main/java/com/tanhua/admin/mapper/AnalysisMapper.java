package com.tanhua.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.tanhua.model.domain.Analysis_by_day;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface AnalysisMapper extends BaseMapper<Analysis_by_day> {

    @Select("SELECT sum(num_registered)  from tb_analysis_by_day  ")
    Integer zhucezongshu();
    @Select("SELECT sum(num_active) from tb_analysis_by_day where record_date=#{format};")
    Integer count(@Param("format") String format);
    @Select("SELECT num_retention1d ,record_date from tb_analysis_by_day  where  record_date>=#{ssdd}  and  record_date <=#{eedd}")
    List<Analysis_by_day> retentionld(@Param("ssdd") String ssdd, @Param("eedd") String eedd);

    @Select("SELECT * from tb_analysis_by_day  where  record_date>=#{sssddd}  and  record_date <=#{eeeddd}")
    List<Analysis_by_day> active(@Param("sssddd")String sssddd, @Param("eeeddd")String eeeddd);
}
